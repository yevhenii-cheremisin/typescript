export const popularBtn = document.getElementById('popular') as HTMLElement;
export const upcomingBtn = document.getElementById('upcoming') as HTMLElement;
export const ratedBtn = document.getElementById('top_rated') as HTMLElement;
export const searchBtn = document.getElementById('submit') as HTMLElement;
export const searchInput = document.getElementById(
    'search'
) as HTMLInputElement;
export const loadmore = document.getElementById(
    'load-more'
) as HTMLInputElement;

export const favorBlock = document.getElementById(
    'favorite-movies'
) as HTMLInputElement;
export const banner = document.getElementById(
    'random-movie'
) as HTMLInputElement;
export const banner_descr = document.getElementById(
    'random-movie-description'
) as HTMLInputElement;
export const banner_title = document.getElementById(
    'random-movie-name'
) as HTMLInputElement;

export const filmContainer = document.getElementById(
    'film-container'
) as HTMLElement;

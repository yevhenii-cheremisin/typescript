const apiPopular: string =
    'https://api.themoviedb.org/3/movie/popular?api_key=27bf3bdf6968c2abb69f4d7d340d96c6&language=en-US&page=';

const apiComming: string =
    'https://api.themoviedb.org/3/movie/upcoming?api_key=27bf3bdf6968c2abb69f4d7d340d96c6&language=en-US&page=';

const apiRated: string =
    'https://api.themoviedb.org/3/movie/top_rated?api_key=27bf3bdf6968c2abb69f4d7d340d96c6&language=en-US&page=';

const apiSearch: string =
    'https://api.themoviedb.org/3/search/movie?api_key=27bf3bdf6968c2abb69f4d7d340d96c6&language=en-US&query=';

export { apiPopular, apiComming, apiRated, apiSearch };

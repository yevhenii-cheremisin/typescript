import { apiPopular, apiComming, apiRated } from './api';
import {
    popularBtn,
    upcomingBtn,
    ratedBtn,
    searchBtn,
    loadmore,
} from './htmlElements';

import { loadPage, ApiSet, searchApiGet, startUp } from './building';

export async function render(): Promise<void> {
    // TODO render your app here

    startUp();

    if (!localStorage.getItem('favourite')) {
        localStorage.setItem('favourite', '[]');
    }

    if (popularBtn) {
        popularBtn.addEventListener('click', function () {
            ApiSet(apiPopular);
        });
    }

    if (upcomingBtn) {
        upcomingBtn.addEventListener('click', function () {
            ApiSet(apiComming);
        });
    }

    if (ratedBtn) {
        ratedBtn.addEventListener('click', function () {
            ApiSet(apiRated);
        });
    }

    if (searchBtn) {
        searchBtn.addEventListener('click', searchApiGet);
    }

    if (loadmore) {
        loadmore.addEventListener('click', loadPage);
    }
}

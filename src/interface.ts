export interface Imovie {
    backdrop_path: string | undefined;
    id: number;
    overview: string;
    poster_path?: string | undefined;
    release_date: string;
    title: string;
}

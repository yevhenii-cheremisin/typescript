import {
    searchInput,
    favorBlock,
    banner,
    banner_descr,
    banner_title,
    filmContainer,
} from './htmlElements';
import { apiPopular, apiSearch } from './api';
import { Imovie } from './interface';

let apiSearchPage = '&page=';
let currentapi: string;
let currentpage: number = 1;

function clearContainer(): void {
    filmContainer.innerHTML = '';
    favorBlock.innerHTML = '';
}

function clearFav(): void {
    favorBlock.innerHTML = '';
}

function mapper(movies: {
    backdrop_path: string | undefined;
    id: number;
    overview: string;
    poster_path: string | undefined;
    release_date: string;
    title: string;
}): Imovie {
    let mapped: Imovie = {
        backdrop_path: movies.backdrop_path,
        id: movies.id,
        overview: movies.overview,
        poster_path: movies.poster_path,
        release_date: movies.release_date,
        title: movies.title,
    };
    return mapped;
}

function filledBlock(info: Imovie, container: HTMLElement): HTMLElement {
    const block = document.createElement('div');
    block.classList.add('card', 'shadow-sm');

    const imgBlock = document.createElement('img');
    imgBlock.src = `https://image.tmdb.org/t/p/original/${info.poster_path}`;

    const xmlns = 'http://www.w3.org/2000/svg';
    const heightSVG: string = '50';
    const widthSVG: string = '50';
    const svgElem = document.createElementNS(xmlns, 'svg');
    svgElem.setAttributeNS(null, 'viewBox', '0 -2 18 22');
    svgElem.setAttributeNS(null, 'width', widthSVG);
    svgElem.setAttributeNS(null, 'height', heightSVG);
    svgElem.setAttributeNS(null, 'stroke', 'red');
    let favouriteArray = JSON.parse(localStorage.getItem('favourite') || '{}');
    if (favouriteArray.includes(info.id)) {
        svgElem.setAttributeNS(null, 'fill', 'red');
    } else {
        svgElem.setAttributeNS(null, 'fill', '#ff000078');
    }

    svgElem.classList.add('bi', 'bi-heart-fill', 'position-absolute', 'p-2');

    if (svgElem) {
        svgElem.addEventListener('click', (event) => {
            likeMovie(svgElem, info.id);
        });
    }

    const path = document.createElementNS(xmlns, 'path');
    path.setAttributeNS(null, 'fill-rule', 'evenodd');
    path.setAttributeNS(
        null,
        'd',
        'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
    );

    svgElem.appendChild(path);

    const textBlock = document.createElement('div');
    textBlock.classList.add('card-body');
    const textP = document.createElement('p');
    textP.classList.add('card-text', 'truncate');
    textP.textContent = info.overview;
    const divTextBlock = document.createElement('div');
    divTextBlock.classList.add(
        'd-flex',
        'justify-content-between',
        'align-items-center'
    );
    const divTextBlockSmall = document.createElement('small');
    divTextBlockSmall.classList.add('text-muted');
    divTextBlockSmall.textContent = info.release_date;
    divTextBlock.appendChild(divTextBlockSmall);
    textBlock.appendChild(textP);
    textBlock.appendChild(divTextBlock);

    block.appendChild(imgBlock);
    block.appendChild(svgElem);
    block.appendChild(textBlock);
    container.appendChild(block);
    return container;
}

function creatingBlock(info: Imovie): void {
    const container = document.createElement('div');
    container.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');

    const filledContainer = filledBlock(info, container);

    filmContainer.appendChild(filledContainer);
}

function creatingFav(info: Imovie): void {
    const container = document.createElement('div');
    container.classList.add('col-12', 'p-2');

    const filledContainer = filledBlock(info, container);

    favorBlock.appendChild(filledContainer);
}

function bannerChanging(info: Imovie): void {
    banner.setAttribute(
        'style',
        `background-image: url('https://image.tmdb.org/t/p/original/${info.backdrop_path}'); height:65vh; background-size: cover;`
    );
    banner_title.textContent = info.title;
    banner_descr.textContent = info.overview;
}

function likeMovie(svgElem: SVGSVGElement, id_movie: number): void {
    if (svgElem.getAttribute('fill') == '#ff000078') {
        svgElem.setAttribute('fill', 'red');
        let favouriteArray = JSON.parse(
            localStorage.getItem('favourite') || '{}'
        );
        favouriteArray.push(id_movie);
        localStorage.setItem('favourite', JSON.stringify(favouriteArray));
    } else {
        svgElem.setAttribute('fill', '#ff000078');
        let favouriteArray = JSON.parse(
            localStorage.getItem('favourite') || '{}'
        );
        favouriteArray = favouriteArray.filter((item: number) => {
            return item != id_movie;
        });
        localStorage.setItem('favourite', JSON.stringify(favouriteArray));
    }
    updateContainer();
    renderFav();
}

function updateContainer(): void {
    clearContainer();
    reloadPage();
}

function reloadPage(): void {
    let api: string = currentapi + 1;
    query(api);
}

function query(api: string): void {
    fetch(api)
        .then(function (res) {
            return res.json();
        })
        .then(function (res) {
            let result: any[] = res.results;
            return result;
        })
        .then(function (res) {
            for (let i = 0; i < res.length; i++) {
                creatingBlock(mapper(res[i]));
            }
        });
}

function getRandomInt(max: number): number {
    return Math.floor(Math.random() * max);
}

export function startUp(): void {
    ClearAll();
    BannerSet();
    ApiSet(apiPopular);
    renderFav();
}

export function ClearAll(): void {
    clearContainer();
    clearFav();
}

export function renderFav(): void {
    clearFav();
    let favouriteArray: string[] = JSON.parse(
        localStorage.getItem('favourite') || '{}'
    );
    for (let i = 0; i < favouriteArray.length; i++) {
        const movie_id = favouriteArray[i];
        fetch(
            `https://api.themoviedb.org/3/movie/${movie_id}?api_key=27bf3bdf6968c2abb69f4d7d340d96c6&language=en-US`
        )
            .then(function (movie) {
                return movie.json();
            })
            .then(function (movie) {
                creatingFav(mapper(movie));
            });
    }
}

export function ApiSet(api: string): void {
    clearContainer();
    currentpage = 1;
    currentapi = api;
    loadPage();
}

export function searchApiGet(): void {
    clearContainer();
    currentpage = 1;
    let query: string = searchInput.value;
    let api: string = apiSearch + query + apiSearchPage;
    currentapi = api;
    loadPage();
}

export function loadPage(): void {
    let api: string = currentapi + currentpage;
    currentpage++;
    query(api);
}

export function BannerSet(): void {
    let pageR: number = getRandomInt(100);
    let movieR: number = getRandomInt(20);
    fetch(apiPopular + pageR)
        .then(function (res) {
            return res.json();
        })
        .then(function (res) {
            let result = res.results[movieR];
            return result;
        })
        .then(function (res) {
            bannerChanging(mapper(res));
        });
}
